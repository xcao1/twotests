#include <iostream>
#include <string>
#include <assert.h>

// ANY OTHER HEADERS HERE
//#include <new>


using namespace std;

#define IS_ANT_LABEL(ch) (((ch) >= '0' && (ch) <= '9') || (ch) == '+')
enum AntDirection { NORTH, EAST, SOUTH, WEST };


const int FIELD_WIDTH = 5;
const int FIELD_HEIGHT = 6;
string AntField[FIELD_HEIGHT] = {
  "...X.",
  "XX.X.",
  "...X.",
  "._.X*",
  "...X.",
  ".....",
};
const int MOUND_X = 1;
const int MOUND_Y = 3;

struct Step{
public:
	int x;
	int y;

	struct Step* mPrior;
	struct Step* mNext;

	Step():x(0),y(0),mPrior(0),mNext(0){}
	Step(int inX, int inY):x(inX),y(inY),mPrior(0),mNext(0)
	{
	}
	~Step(){
		delete mNext;
		mNext = 0;
		mPrior = 0;
	}
};

struct Ant {
public:
	int x, y; // 0-based
	char label;
	AntDirection preferred_dir;
	bool have_food;

	// SOME METHOD OF SAVING HISTORY HERE
	bool inTheFeild;
	Step *mPath;
	Step *mCurrent;
	bool visited[FIELD_HEIGHT][FIELD_WIDTH];

	Ant() { 
		x = y = label = 0; 
		preferred_dir = NORTH; 
		have_food = false;
		inTheFeild = false;

		mPath = 0;
		for(int i = 0; i != FIELD_HEIGHT; i ++)
		{
			for(int j = 0; j != FIELD_WIDTH; j ++)
			{
				visited[i][j] = false;
			}
		}
	}
	~Ant(){
		delete mPath;
		mPath = 0;
		mCurrent = 0;
	}
};
const int MAX_ANTS = 8;
Ant ants[MAX_ANTS];
int num_ants = 0;

string fieldToString()
{
	string field_copy[FIELD_HEIGHT];
	copy(AntField, AntField+FIELD_HEIGHT, field_copy);
	for (int a = 0; a < num_ants; a++)
		if (IS_ANT_LABEL(field_copy[ants[a].y][ants[a].x]))
			field_copy[ants[a].y][ants[a].x] = '+';
		else
			field_copy[ants[a].y][ants[a].x] = ants[a].label;
	string result;
	for (int y = 0; y < FIELD_HEIGHT; y++)
	{
		result += field_copy[y];
		result += "\n";
	}
	return result;
}

void tickMound()
{
	// FILL IN CODE HERE
	if(num_ants >= 0 && num_ants <= 7 && num_ants < MAX_ANTS)
	{// generate ant when number in the range
		
		// set to the mound position
		ants[num_ants].x = 1;
		ants[num_ants].y = 3;
		ants[num_ants].label = '0' + num_ants;
		ants[num_ants].inTheFeild = true;

		do{
			ants[num_ants].mPath = new(nothrow) Step(1,3);
		}while(ants[num_ants].mPath == 0);
		ants[num_ants].mCurrent = ants[num_ants].mPath;
		
		
		int dir = num_ants%4;
		switch(dir)
		{
		case 0:// north
			ants[num_ants].preferred_dir = NORTH;
			break;
		case 1:// east
			ants[num_ants].preferred_dir = EAST;
			break;
		case 2:// south
			ants[num_ants].preferred_dir = SOUTH;
			break;
		case 3:// west
			ants[num_ants].preferred_dir = WEST;
			break;
		default:
			cerr<<"error : no such direction.";
			break;
		}
		// count the number
		num_ants ++;
	}
}

bool tryWalk(Ant& ant, AntDirection dir)
{
	// FILL IN CODE HERE
	// get current ant's position
	int attemptX = ant.x;
	int attemptY = ant.y;

	switch(dir)
	{
	case NORTH:// move up
		attemptY -= 1;
		if( attemptY < 0 || attemptY >= FIELD_HEIGHT )// unable to move
			return false;
		else
		{
			if( AntField[attemptY][attemptX] == 'X' )// unable to move
				return false;
			else
			{
				if(ant.visited[attemptY][attemptX] == true)
					return false;
				else
				{
					if( attemptX == 1 && attemptY == 3 )
						return false;
					else// able to move
						return true;
				}
			}
		}
		break;
	case EAST:// move right
		attemptX += 1;
		if( attemptX < 0 || attemptX >= FIELD_WIDTH )// unable to move
			return false;
		else
		{
			if( AntField[attemptY][attemptX] == 'X' )// unable to move
				return false;
			else
			{
				if(ant.visited[attemptY][attemptX] == true)
					return false;
				else
				{
					if( attemptX == 1 && attemptY == 3 )
						return false;
					else// able to move
						return true;
				}
			}
		}
		break;
	case SOUTH:// move down
		attemptY += 1;
		if( attemptY < 0 || attemptY >= FIELD_HEIGHT )// unable to move
			return false;
		else
		{
			if( AntField[attemptY][attemptX] == 'X' )// unable to move
				return false;
			else
			{
				if(ant.visited[attemptY][attemptX] == true)
					return false;
				else
				{
					if( attemptX == 1 && attemptY == 3 )
						return false;
					else// able to move
						return true;
				}
			}
		}
		break;
	case WEST:// move left
		attemptX -= 1;
		if( attemptX < 0 || attemptX >= FIELD_WIDTH )// unable to move
			return false;
		else
		{
			if( AntField[attemptY][attemptX] == 'X' )// unable to move
				return false;
			else
			{
				if(ant.visited[attemptY][attemptX] == true)
					return false;
				else
				{
					if( attemptX == 1 && attemptY == 3 )
						return false;
					else// able to move
						return true;
				}
			}
		}
		break;
	default:
		cerr<<"error : no such direction.";
		return false;
		break;
	}
}

void tickAnts()
{
	// FILL IN CODE HERE
	// find out ants in the feilds
	for(int i = 0; i != MAX_ANTS; i ++)
	{// for each ant
		if(ants[i].inTheFeild == true)
		{// this ant is in the feild
			if(ants[i].have_food == true)
			{// have food, go home
				ants[i].mCurrent = ants[i].mCurrent->mPrior;
				ants[i].x = ants[i].mCurrent->x;
				ants[i].y = ants[i].mCurrent->y;

				if(ants[i].x == 1 && ants[i].y == 3 )
				{// reach home
					// forget everything
					ants[i].have_food = false;
					delete ants[i].mPath;
					ants[i].mPath = 0;

					
					do{
						ants[i].mPath = new(nothrow) Step(1,3);
					}while( ants[i].mPath == 0 );
					
					do{
						ants[i].mCurrent = 0;
						ants[i].mCurrent = ants[i].mPath;
					}while( ants[i].mCurrent == 0 );
					

					for(int l = 0; l != FIELD_HEIGHT; l ++)
					{
						for(int m = 0; m != FIELD_WIDTH; m ++)
						{
							ants[i].visited[l][m] = false;
						}
					}
				}
			}
			else
			{// find food
				AntDirection dir = ants[i].preferred_dir;
				bool ableToMove = true;
				while( tryWalk(ants[i],dir) != true )
				{// while the direction is not able to move to
					// change to the next direction
					if(dir == WEST)
						dir = NORTH;
					else
						dir = AntDirection (dir + 1);
					// while reach default preferred direction
					if( dir == ants[i].preferred_dir )
					{
						// unable to move
						ableToMove = false;
						break;
					}
				}
				if( ableToMove == true )
				{// able to move
					switch(dir)
					{
					case NORTH:// move up
						do{
							ants[i].mCurrent->mNext = new(nothrow) Step(ants[i].x, ants[i].y-1);
						}while(ants[i].mCurrent->mNext == 0);
						ants[i].mCurrent->mNext->mPrior = ants[i].mCurrent;
						ants[i].mCurrent = ants[i].mCurrent->mNext;

						ants[i].y -= 1;
						ants[i].visited[ants[i].y][ants[i].x] = true;
						break;
					case EAST:// move right
						do{
							ants[i].mCurrent->mNext = new(nothrow) Step(ants[i].x+1, ants[i].y);
						}while(ants[i].mCurrent->mNext == 0);
						ants[i].mCurrent->mNext->mPrior = ants[i].mCurrent;
						ants[i].mCurrent = ants[i].mCurrent->mNext;

						ants[i].x += 1;
						ants[i].visited[ants[i].y][ants[i].x] = true;
						break;
					case SOUTH:// move down
						do{
							ants[i].mCurrent->mNext = new(nothrow) Step(ants[i].x, ants[i].y+1);
						}while(ants[i].mCurrent->mNext == 0);
						ants[i].mCurrent->mNext->mPrior = ants[i].mCurrent;
						ants[i].mCurrent = ants[i].mCurrent->mNext;

						ants[i].y += 1;
						ants[i].visited[ants[i].y][ants[i].x] = true;
						break;
					case WEST:// move left
						do{
							ants[i].mCurrent->mNext = new(nothrow) Step(ants[i].x-1, ants[i].y);
						}while(ants[i].mCurrent->mNext == 0);
						ants[i].mCurrent->mNext->mPrior = ants[i].mCurrent;
						ants[i].mCurrent = ants[i].mCurrent->mNext;

						ants[i].x -= 1;
						ants[i].visited[ants[i].y][ants[i].x] = true;
						break;
					default:
						cerr<<"error : no such direction.";
						break;
					}
					if(ants[i].x == 4 && ants[i].y == 3)
					{// reach the food
						ants[i].have_food = true;
					}
				}
				// if not able to move any more, stay
			}
		}
	}
}

void printField()
{
	cout<<"\n"<<fieldToString()<<endl;
}
void timeStep(int numsteps)
{
	printField();
	//set_new_handler(tickMound);
	for (int i = 0; i < numsteps; i++)
	{
		string goOn;
		cin>>goOn;
		
		
		tickMound();
		tickAnts();
		printField();
		
	}
}


int main()
{
	timeStep(5);//cout<<".0.X.\nXX.X.\n34.X.\n._.X*\n...X.\n2..1.\n";
	assert(fieldToString() == ".0.X.\nXX.X.\n34.X.\n._.X*\n...X.\n2..1.\n");

	timeStep(5);//cout<<"+3.X.\nXX.X.\n.72X.\n._.X*\n6..X.\n....+\n";
	assert(fieldToString() == "+3.X.\nXX.X.\n.72X.\n._.X*\n6..X.\n....+\n");

	timeStep(5);//cout<<"+..X.\nXX.X.\n...X.\n.16X*\n...X.\n...52\n";
	assert(fieldToString() == "+..X.\nXX.X.\n...X.\n.16X*\n...X.\n...52\n");

	timeStep(5);cout<<"+..X.\nXX.X.\n...X.\n._5X*\n...X6\n...21\n";
	assert(fieldToString() == "+..X.\nXX.X.\n...X.\n._5X*\n...X6\n...21\n");

	timeStep(5);
	assert(fieldToString() == "+..X.\nXX.X.\n.2.X.\n._.X*\n...X5\n..61.\n");

	return 0;
}