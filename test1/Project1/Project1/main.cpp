#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

// ================================
// CombatVariables
struct CombatVariables
{
public:
	int hit_points;
	// going to be a large struct

public:
	CombatVariables();
	~CombatVariables();
};
CombatVariables::CombatVariables(): hit_points(0)
{
}
CombatVariables::~CombatVariables()
{
}

// =================================
// Entity
struct Entity
{
private:
	string mName;
protected:
	bool mCanDraw;
	CombatVariables mCombatVariables;
public:
	Entity(string name);
	~Entity();

	// return the name of this Entity
	string name();
	// return the hit points of this Entity
	int hit_points();
	// return if this Entity can be drawn
	bool can_draw();
};
Entity::Entity(string name): mName(name), mCanDraw(true), mCombatVariables()
{
}
Entity::~Entity()
{
}
// name
string Entity::name()
{
	return mName;
}
// hit_points
int Entity::hit_points()
{
	return mCombatVariables.hit_points;
}
// can_draw
bool Entity::can_draw()
{
	return mCanDraw;
}

// ===============================
// ScriptObject
struct ScriptObject: Entity
{
public:
	ScriptObject(string name);
};
ScriptObject::ScriptObject(string name): Entity(name)
{
	// can not be drawn
	mCanDraw = false;
}

// ===============================
// NPC
struct NPC: Entity
{
public:
	NPC(string name);
};
NPC::NPC(string name): Entity(name)
{
}
// ===============================
// Monster
struct Monster: NPC
{
private:
public:
	Monster(string name);
	~Monster();
};
Monster::Monster(string name): NPC(name)
{
	mCombatVariables.hit_points = 20;
}
Monster::~Monster()
{
}
// ===============================
// Player
struct Player: Entity
{
private:
public:
	Player(string name);
	~Player();
};
Player::Player(string name): Entity(name)
{
	mCombatVariables.hit_points = 100;
}
Player::~Player()
{
}



// =======================================
// main function
int main()
{
	NPC npc("Bartender");
	Monster monster("Goblin");
	ScriptObject script("Detector");
	Player player("Leroy Jenkins");

	assert(npc.name() == "Bartender");
	assert(monster.name() == "Goblin");
	assert(script.name() == "Detector");
	assert(player.name() == "Leroy Jenkins");

	assert(npc.hit_points() == 0);
	assert(monster.hit_points() == 20);
	assert(script.hit_points() == 0);
	assert(player.hit_points() == 100);

	assert(npc.can_draw() == true);
	assert(monster.can_draw() == true);
	assert(script.can_draw() == false);
	assert(player.can_draw() == true);

	return 0;
}